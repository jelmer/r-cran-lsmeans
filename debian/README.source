Explanation for binary files inside source package according to
  http://lists.debian.org/debian-devel/2013/09/msg00332.html

This package contains many example data sets

Files: data/autonoise.RData
Documentation: man/auto.noise.Rd
  Example data set:
  Three-factor experiment comparing pollution-filter noise for two filters,
  three sizes of cars, and two sides of the car.
  The data are from a statement by Texaco, Inc., to the Air and Water Pollution
  Subcommittee of the Senate Public Works Committee on June 26, 1973.    
  Mr. John McKinley, President of Texaco, cited an automobile filter developed
  by Associated Octel Company as effective in reducing pollution. However, 
  questions had been raised about the effects of filters on vehicle performance, 
  fuel consumption, exhaust gas back pressure, and silencing. On the last 
  question, he referred to the data included here as evidence that the silencing
  properties of the Octel filter were at least equal to those of standard
  silencers.

Files: data/feedlot.RData
Documentation: man/feedlot.Rd
  Example data set:
  This is an unbalanced analysis-of-covariance example, where one covariate is
  affected by a factor. Feeder calves from various herds enter a feedlot, where
  they are fed one of three diets. The weight of the animal at entry is the
  covariate, and the weight at slaughter is the response.

Files: data/fiber.RData
Documentation: man/fiber.Rd
  Example data set:
  Fiber data from Montgomery Design (8th ed.), p.656 (Table 15.10). Useful as a
  simple analysis-of-covariance example.
  The goal of the experiment is to compare the mean breaking strength of fibers
  produced by the three machines. When testing this, the technician also
  measured the diameter of each fiber, and this measurement may be used as a
  concomitant variable to improve precision of the estimates.

Files: data/MOats.RData
Documentation: man/MOats.Rd
  Example data set:
  This is the Oats dataset provided in the nlme package, but it is rearranged
  as one multivariate observation per plot.

Files: data/nutrition.RData
Documentation: man/nutrition.Rd
  Example data set:
  This observational dataset involves three factors, but where several factor
  combinations are missing. It is used as a case study in Milliken and Johnson,
  Chapter 17, p.202. (You may also find it in the second edition, p.278.)
  A survey was conducted by home economists to study how much lower
  socioeconomic-level mothers knew about nutrition and to judge the effect of a
  training program designed to increase therir knowledge of nutrition. This is
  a messy dataset with several empty cells.

Files: data/oranges.RData
Documentation: man/oranges.Rd
  Example data set:
  This example dataset on sales of oranges has two factors, two covariates, and
  two responses. There is one observation per factor combination.


